DELETE FROM prodms.goods;

ALTER SEQUENCE prodms.goods_id_seq RESTART WITH 1;

INSERT INTO prodms.goods
		(goods_name,manufacturer,price,description)
	VALUES 
		('Abacuses','ASRock',100,'Cases'),
		('Bags','Compaq',200,'Laptop computer cases'),
		('Capacitors','Dell',300,'Motherboards'),
		('Detectors','Foxconn',400,'Chipsets for motherboards'),
		('Lyres','Lenovo',500,'Central processing units (CPUs)');
