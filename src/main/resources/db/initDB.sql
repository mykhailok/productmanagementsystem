DROP TABLE IF EXISTS prodms.goods CASCADE;

CREATE TABLE prodms.goods
(
    id serial NOT NULL,
    goods_name TEXT,
    manufacturer TEXT,
    price NUMERIC(10,2),
    description TEXT,
    PRIMARY KEY (id)
);

GRANT SELECT ON ALL TABLES IN SCHEMA pms TO "user";
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA pms TO "user";
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA pms to "user";