package ua.goit.javaee.group2.model;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(name = Goods.DELETE_BY_ID, query = "DELETE FROM Goods g WHERE g.id=:id"),
        @NamedQuery(name = Goods.DELETE_ALL, query = "DELETE FROM Goods g"),
        @NamedQuery(name = Goods.LOAD_ALL, query = "SELECT g FROM Goods g"),
        @NamedQuery(name = Goods.LOAD_BY_GOODS_NAME, query = "SELECT c FROM Company c WHERE c.name=:name")
})

@Entity
@Table(name = "goods")
@AttributeOverride(name = "name", column = @Column(name = Goods.NAME))
public class Goods extends NamedEntity{

    public static final String NAME = "goods_name";

    public static final String DELETE_BY_ID = "Goods.delete";
    public static final String DELETE_ALL = "Goods.deleteAll";
    public static final String LOAD_ALL = "Goods.loadAll";
    public static final String LOAD_BY_GOODS_NAME = "Goods.findByName";

    @Column(name = "manufacturer")
    private String manufacturer;

    @Column(name = "price")
    private float price;

    @Column(name = "description")
    private String description;


    public Goods() {
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "ID='" + getID() + '\'' +
                "goods_name='" + getName() + '\'' +
                "manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
