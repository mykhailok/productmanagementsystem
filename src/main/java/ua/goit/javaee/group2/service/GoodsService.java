package ua.goit.javaee.group2.service;



import java.sql.SQLException;
import java.util.List;



public abstract class GoodsService<T> {

    public abstract T add(T t) throws SQLException;

    public abstract T getById(int id) throws SQLException;

    public abstract List<T> getAll() throws SQLException;

    public abstract void update(T t) throws SQLException;

    public abstract void deleteById(int id) throws SQLException;

    public abstract void deleteAll() throws SQLException;

}
