package ua.goit.javaee.group2.service;


import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.goit.javaee.group2.dao.GoodsDAO;
import ua.goit.javaee.group2.model.Goods;

import java.sql.SQLException;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;


@Service
public class GoodsServiceImpl extends GoodsService<Goods> {

    private GoodsDAO goodsDAO;

    private static final Logger LOGGER = getLogger(GoodsServiceImpl.class);

    @Override
    @Transactional
    public Goods add(Goods goods) throws SQLException {
        Goods goodsSerchedByName = goodsDAO.findByName(goods.getName());

        if (goodsSerchedByName == null) {
            goodsDAO.save(goods);
            return goods;
        } else{
            LOGGER.error("Goods " + goods + " wasn't saved to database." +
                    " There is an identical goods in the database already.");
            return goodsSerchedByName;
        }
    }

    @Override
    @Transactional
    public Goods getById(int id) throws SQLException {
        return goodsDAO.findById(id);
    }

    @Override
    @Transactional
    public List getAll() throws SQLException {
        return goodsDAO.findAll();
    }

    @Override
    @Transactional
    public void update(Goods goods) throws SQLException {
        goodsDAO.save(goods);
    }

    @Override
    @Transactional
    public void deleteById(int id) throws SQLException {
        goodsDAO.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteAll() throws SQLException {
        goodsDAO.deleteAll();
    }

    public void setGoodsDAO(GoodsDAO goodsDAO) {
        this.goodsDAO = goodsDAO;
    }

}
