package ua.goit.javaee.group2.dao;

import ua.goit.javaee.group2.model.Goods;

public interface GoodsDAO extends AbstractDAO<Goods> {
    Goods findByName(String name);
}
