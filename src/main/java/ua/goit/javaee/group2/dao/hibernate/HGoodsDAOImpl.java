package ua.goit.javaee.group2.dao.hibernate;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.springframework.stereotype.Repository;
import ua.goit.javaee.group2.dao.GoodsDAO;
import ua.goit.javaee.group2.model.Goods;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

//@Repository
public class HGoodsDAOImpl implements GoodsDAO {

    private static final Logger LOGGER = getLogger(HGoodsDAOImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public Goods save(Goods goods) {
        boolean isNew = goods.isNew();
        if (isNew) {
            em.persist(goods);
        } else {
            em.merge(goods);
        }
        LOGGER.info(goods + " was " + (isNew ? "created" : "updated") + " successfully.");
        return goods;
    }

    @Override
    public void saveAll(List<Goods> list) {
        list.forEach(em::persist);
        LOGGER.info("All goods were saved successfully.");
    }

    @Override
    public Goods findById(int id) {
        Goods goods = em.find(Goods.class, id);
        if (goods == null) {
            LOGGER.error("Cannot find company by id ID: " + id);
        } else {
            LOGGER.info(goods + " successfully loaded from DB by ID.");
        }
        return goods;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Goods> findAll() {
        List<Goods> goodsList = (List<Goods>) em.
                createNamedQuery(Goods.LOAD_ALL).getResultList();
        if (goodsList == null) {
            LOGGER.error("Search for goods has failed.");
        } else {
            LOGGER.info("Search for all goods has been successful.");
        }
        return goodsList;
    }

    @Override
    public void deleteById(int id) {
        Query query = (Query) em.createNamedQuery(Goods.DELETE_BY_ID);
        query.setParameter("id", id);
        if (query.executeUpdate() == 1) {
            LOGGER.info("Deleting goods by ID has been successful.");
        } else {
            LOGGER.error("Deleting goods by ID has failed.");
        }
    }

    @Override
    public void deleteAll() {
        if (em.createNamedQuery(Goods.DELETE_ALL).executeUpdate() != 0) {
            LOGGER.info("Deleting of all goods has been successful.");
        } else {
            LOGGER.error("Deleting of all goods has failed.");
        }
    }

    @Override
    public Goods findByName(String name) {
        Query query = (Query) em.createNamedQuery(Goods.LOAD_BY_GOODS_NAME);
        query.setParameter("name", name);
        Goods company = null;
        if (query.getResultList().size() == 0) {
            LOGGER.error("Cannot find company by name: " + name);
        } else {
            company = (Goods) query.getSingleResult();
            LOGGER.error("Goods has been successfully found by name: " + name);
        }
        return company;
    }
}
