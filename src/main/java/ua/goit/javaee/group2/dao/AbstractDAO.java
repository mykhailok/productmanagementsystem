package ua.goit.javaee.group2.dao;

import java.util.List;

public interface AbstractDAO<T> {

    T save(T t);

    void saveAll(List<T> list);

    T findById(int id);

    List<T> findAll();

    void deleteById(int id);

    void deleteAll();

}
